module ApplicationHelper
  def format_duration(item)
    if item.all_day?
      format_date_duration(item)
    else
      format_datetime_duration(item)
    end
  end

  private def format_date_duration(item)
    w1 = format_wday(item.starts_at)
    w2 = format_wday(item.ends_at)
    s = ''
    s << if item.starts_at.year == Time.current.year
           item.starts_at.strftime("%-m月%-d日 （#{w1}）")
         else
           item.starts_at.strftime("%Y年%-m月%-d日 （#{w1}）")
         end
    if item.starts_at != item.ends_at.advance(days: -1)
      s << ' 〜 '
      s << if item.ends_at.year == item.starts_at.year
             item.ends_at.strftime("%-m月%-d日（#{w2}）")
           else
             item.ends_at.strftime("%Y年%-m月%-d日（#{w2}）")
           end
    end
    s
  end

  private def format_datetime_duration(item)
    w1 = format_wday(item.starts_at)
    w2 = format_wday(item.ends_at)
    s = ''
    s << if item.starts_at.year == Time.current.year
           item.starts_at.strftime("%-m月%-d日 （#{w1}） %H:%M")
         else
           item.starts_at.strftime("%Y年%-m月%-d日 （#{w1}） %H:%M")
         end
    s << ' 〜 '
    s << if item.ends_at.to_date == item.starts_at.to_date
           item.ends_at.strftime('%H:%M')
         elsif item.ends_at.year == item.starts_at.year
           item.ends_at.strftime("%-m月%-d日 （#{w2}） %H:%M")
         else
           item.ends_at.strftime("%Y年%-m月%-d日 （#{w2}） %H:%M")
         end
  end

  def format_wday(datetime)
    %w[日 月 火 水 木 金 土][datetime.wday]
  end

  def format_datetime(datetime)
    w = format_wday(datetime)
    datetime.strftime("%Y年%-m月%-d日 (#{w}) %H:%M")
  end

  def format_date(date)
    w = format_wday(date)
    date.strftime("%Y年%-m月%-d日（#{w}）")
  end

  def document_title
    'PicoPlanner'
  end
end
