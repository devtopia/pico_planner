class PlanItemsController < ApplicationController
  def index
    @plan_items = PlanItem.natural_order
  end

  # GET (collection)
  def of_today
    t0 = Time.current.beginning_of_day
    t1 = t0.advance(hours: 24)
    @plan_items = PlanItem
      .where('starts_at >= ? AND starts_at < ?', t0, t1)
      .or(PlanItem.where('ends_at > ? AND ends_at <= ?', t0, t1))
      .natural_order
    @continued_plan_items = PlanItem
      .where('starts_at < ? AND ends_at > ?', t0, t1)
      .natural_order
    render action: :index
  end

  def show
    @plan_item = PlanItem.find(params[:id])
  end

  def new
    @plan_item = PlanItem.new
    time0 = Time.current.beginning_of_hour
    @plan_item.starts_at = time0.advance(hours: 1)
    @plan_item.ends_at = time0.advance(hours: 2)
  end

  def edit
    @plan_item = PlanItem.find(params[:id])
  end

  def create
    @plan_item = PlanItem.new(plan_item_params)
    if @plan_item.save
      redirect_to :plan_items, status: 301, notice: '予定を追加しました。'
    else
      render action: 'new'
    end
  end

  def update
    @plan_item = PlanItem.find(params[:id])
    @plan_item.assign_attributes(plan_item_params)

    if @plan_item.save
      redirect_to :plan_items, notice: '予定を変更しました。'
    else
      render action: 'edit'
    end
  end

  def destroy
    @plan_item = PlanItem.find(params[:id])
    @plan_item.destroy!
    redirect_to :plan_items, notice: '予定を削除しました。'
  end

  private def plan_item_params
    params[:plan_item].permit(:name, :description, :starts_at, :ends_at, :all_day)
  end
end
