# == Schema Information
#
# Table name: plan_items
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  description :text(65535)      not null
#  all_day     :boolean          default(FALSE), not null
#  starts_at   :datetime         not null
#  ends_at     :datetime         not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class PlanItem < ApplicationRecord
  scope :natural_order, -> { order(starts_at: :asc, all_day: :desc, id: :asc) }

  validates :name, presence: true, length: { maximum: 80 }
  validates :description, length: { maximum: 400 }
  validates :starts_at, :ends_at, presence: true

  validate do
    if starts_at && ends_at && starts_at > ends_at
      errors.add(:ends_at, :too_early)
    end
  end

  before_save do
    self.starts_at = starts_at if starts_at
    self.ends_at = ends_at if ends_at

    if all_day?
      self.starts_at = starts_at.beginning_of_day
      self.ends_at = ends_at.tomorrow.beginning_of_day - 1
    end
  end
end
