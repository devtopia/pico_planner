Rails.application.config.generators do |g|
  g.orm :active_record
  g.test_framework :rspec, :fixture => true
  g.fixture_replacement :factory_bot, :dir => "spec/factories"
  g.view_specs false
  g.controller_specs false
  g.routing_specs false
  g.helper_specs false
  g.request_specs false
  g.assets false
  g.helper false
  g.skip_routes true
end
