const { environment } = require('@rails/webpacker')
const webpack = require('webpack')
const customConfig = require('./custom')

environment.plugins.set(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    tether: 'tether',
    Tether: 'tether',
    'window.Tether': 'tether',
    Popper: ['popper.js', 'default']
  })
)

environment.loaders.get('sass').use.push('import-glob-loader')
environment.config.merge(customConfig)

module.exports = environment
